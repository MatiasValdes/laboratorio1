#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Se define la clase velocimetro
class Velocimetro():
    # El velicimetro parte en 0
    def __init__(self):
        self.velocimetro = 0

    # Metodo para obtener la velocidad
    def get_velocidad(self):
        return self.velocimetro

    # Metodo para generar la velocidad aleatoriamente
    def set_velocidad(self):
        self.velocimetro = random.randint(0, 200)
