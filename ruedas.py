#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Se crea la clase de ruedas
class Ruedas():
    # Se definen las ruedas segun el valor principal
    def __init__(self, valor):
        self.ruedas = valor

    # Metodo para obtener el estado de las ruedas en cada momento
    def get_rueda(self):
        return self.ruedas

    # Metodo para actualizar el valor de las ruedas
    def set_rueda(self, valor):
        self.ruedas = valor

    # Metodo que ve el deterioro de las ruedas después de cada movimiento
    def deterioro(self):
        # El deterioro es aleatorio
        deterioro = random.randint(1, 10)
        # Le restamos el deterioro al valor de nuestros ruedas
        self.ruedas = self.ruedas - deterioro
        # Si tiene un deterioro sobre el 90% se reemplazan
        if self.ruedas < 10:
            print("Se cambio en los pits")
            self.ruedas = 100

        return self.ruedas
