#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Se define la clase motor
class Motor():
    # Se crea por constructor por defecto
    def __init__(self):
        self.motor = None

    # Metodo para obtener el motor
    def set_motor(self):
        # Se crea una lista con los 2 motores posibles
        motores = [1.2, 1.6]
        # y se extrae el motor aleatoriamente y se define dentro de la misma
        # clase
        motor = random.choice(motores)
        self.motor = motor

    # Metodo para obtener el motor en cualquier momento
    def get_motor(self):
        return self.motor
