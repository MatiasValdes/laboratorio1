#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Se define la clase estanque
class Estanque():
    # El estanque inicial será de 32 litros
    def __init__(self):
        self.estanque = 32

    # Metodo para obtener el estanque en cada momento
    def get_estanque(self):
        return self.estanque
