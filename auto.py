#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from estanque import Estanque
from motor import Motor
from ruedas import Ruedas
from velocimetro import Velocimetro
import random


# Se crea la clase auto
class Auto():
    # Se inicia con el unico parametro que es entregado por el usuario
    def __init__(self, nombre):
        self.nombre = nombre
        self.valor = 100
        self.motor = None
        # Definimos dos objetos tanto la cilindrada como el estanque
        self.cilindrada = Motor()
        self.bencina = Estanque()
        self.estanque = None
        # Además definimos 5 ruedas como objetos
        self.rueda = Ruedas(self.valor)
        self.rueda1 = Ruedas(self.valor)
        self.rueda2 = Ruedas(self.valor)
        self.rueda3 = Ruedas(self.valor)
        self.rueda4 = Ruedas(self.valor)
        # La velocidad también será un objeto
        self.velocidad = Velocimetro()
        self.velocimetro = self.velocidad.get_velocidad()
        self.kilometros = 0

    # Metodo para obtener el motor del auto
    def set_motor(self):
        self.cilindrada.set_motor()
        new = self.cilindrada.get_motor()
        # Con esto obtendremos el motor de nuestro auto
        self.motor = new

    # Metodo para obtener nuestro estanque inicial
    def set_estanque_inicial(self):
        litros = self.bencina.get_estanque()
        self.estanque = litros

    # Metodo para obtener el estanque luego de que el auto se enciende
    def set_estanque(self):
        # Gastará 1% de bencina cuando se encienda
        arranque = (self.estanque) * 0.01
        self.estanque = (self.estanque - arranque)

    # Metodo para obtener el modelo del motor
    def get_motor(self):
        return self.motor

    # Metodo más importante que hará que nuestro auto se mueva
    def movimiento(self, andar):
        # Si el booleano que se entrega es distinto a False, el auto se movera
        if andar is not False:
            # Se crea una velocidad aleatoria
            # self.velocimetro = random.randint(0, 200)
            self.velocidad.set_velocidad()
            self.velocimetro = self.velocidad.get_velocidad()
            # Si la velocidad es mayor a 120, debe bajar la velocidad y se
            # muestra en pantalla como el auto debe bajar la velocidad
            if self.velocimetro > 120:
                print("El auto va muy rapido, baje la velocidad")
                print("Llevaba una velocidad de {0} Km/h".format
                      (self.velocimetro))
                pass

            # Si está en el rango
            else:
                # Vemos que cilindrada tiene el motor
                if self.motor == 1.2:
                    # Se define un tiempo al azar
                    tiempo = random.randint(1, 10)
                    # Se saca la distancia que recorre y se agrega a la
                    # distancia recorrida
                    distancia = (tiempo * self.velocimetro)
                    self.kilometros = self.kilometros + distancia
                    # Además sacamos los litros gastados en el movimiento
                    gastado = distancia / 20
                    self.estanque = self.estanque - gastado
                    # El estanque nunca será negativo por ello lo dejamos en 0
                    if self.estanque < 0:
                        self.estanque = 0
                    # A cada rueda llamamos su metodo para ver el deterioro
                    self.rueda1.deterioro()
                    self.rueda2.deterioro()
                    self.rueda3.deterioro()
                    self.rueda4.deterioro()
                    # Llamamos al propio metodo de la clase auto para obtener
                    # el estado del auto
                    self.listar_auto()

                # Si no es cilindrada 1.2, será 1.6
                else:
                    # Nuevamente hacemos el mismo proceso que para la
                    # cilindrada 1.2
                    tiempo = random.randint(1, 10)
                    distancia = (tiempo * self.velocimetro)
                    self.kilometros = self.kilometros + distancia
                    # Esta vez el gasto de bencina será distinto que con la
                    # cilindrada 1.6
                    gastado = distancia / 14
                    self.estanque = self.estanque - gastado
                    if self.estanque < 0:
                        self.estanque = 0
                    self.rueda1.deterioro()
                    self.rueda2.deterioro()
                    self.rueda3.deterioro()
                    self.rueda4.deterioro()
                    self.listar_auto()
        else:
            print("El auto entra en reposo.")

    # Metodo para obtener el estanque del auto en cada momento
    def get_estanque(self):
        return self.estanque

    # Metodo que nos permite ver el estado del auto
    def listar_auto(self):
        # Definimos cada rueda como una letra del abecedario
        a = self.rueda1.get_rueda()
        b = self.rueda2.get_rueda()
        c = self.rueda3.get_rueda()
        d = self.rueda4.get_rueda()
        # Imprimimos el estado del auto
        print("El auto ha recorrido {0} kilometros, se mueve actualmente a "
              "{1} Km/h, en su estanque aun quedan {2} litros de bencina\n"
              "Las condiciones de las ruedas son:\n╬╬ {3}% ╬╬ {4}% ╬╬ {5}% ╬╬"
              " {6} % ╬╬".format(self.kilometros, self.velocimetro,
                                 self.estanque, a, b, c, d))

    # Metodo que entrega el estado final de las ruedas
    def estado_final(self):
        # Definimos cada rueda como una letra del abecedario
        a = self.rueda1.get_rueda()
        b = self.rueda2.get_rueda()
        c = self.rueda3.get_rueda()
        d = self.rueda4.get_rueda()
        # Imprimimos el estado del auto
        print("\n\nEl estado final del auto es:\n{0} litros de bencina. Las"
              "condiciones de las ruedas finales fueron: \n ╬╬ {1}% ╬╬"
              " ╬╬ {2}% ╬╬ {3}% ╬╬ {4} ╬╬".format(self.estanque, a, b, c, d))
