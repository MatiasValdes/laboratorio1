#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Importamos clases y funciones que nos servirán para nuestro codigo
from auto import Auto
import os
import time


# Se define funcion para borrar la pantalla de ejecucion
def borrarPantalla():
    if os.name == "posix":
        os.system("clear")


# Funcion principal
def main():
    # Pedimos un unico dato para nuestro auto
    nombre = str(input("Pongale nombre a su auto o el modelo de este: \n"))
    # Creamos nuestro auto con algunos atributos que serán utilizados
    nombre = Auto(nombre)
    nombre.set_motor()
    nombre.set_estanque_inicial()
    # Hacemos ver como está nuestro auto inicialmente
    print("{0} es el nombre de su auto, actualmente posee un total de {1}"
          " litros de bencina y posee un motor de {2} de cilindrada"
          .format(nombre.nombre, nombre.get_estanque(), nombre.get_motor()))

    # Damos un intervalo de 5 segundos y luego borramos la pantalla
    time.sleep(5)
    borrarPantalla()
    contador = 0
    # Obtenemos la bencina inicial de nuestro auto para iniciar el ciclo
    bencina = nombre.get_estanque()
    while bencina > 0:
        # Pedimos ver que hará el auto en cada movimiento, si moverse o
        # simplemente quedarse en reposo
        indicador = input("Presione:\n1)Para mover el vehiculo\n"
                          "2)Para detener el vehiculo: \n")
        try:
            indicador = int(indicador)
            if indicador == 1:
                # Si es la primera vez que se mueve se gastará el 1% de echarlo
                # a andar
                if contador == 0:
                    nombre.set_estanque()
                    arranque = True
                    # Enviamos el auto al metodo que hará que el auto arranque
                    nombre.movimiento(arranque)
                    # Definimos un contador disnto de 0 porque ya está en
                    # movimiento
                    contador = 10
                else:
                    # Para cualquier movimiento después del inicial solo se
                    # llama al metodo
                    arranque = True
                    nombre.movimiento(arranque)

            elif indicador == 2:
                # Si el usuario no quiere que se mueva el auto, llamamos al
                # metodo de la clase auto con el arranque en False, además
                # reiniciamos el contador a 0 ya que el auto entrará en reposo
                # nuevamente
                arranque = False
                nombre.movimiento(arranque)
                contador = 0
                print("Se detuvo el auto")

            # Damos un intervalo de 5 segundos para observar lo que pasa en el
            # auto para luego borrar pantalla y obtener la nueva bencina porque
            #  se gastó bencina en el movimiento
            time.sleep(5)
            borrarPantalla()
            bencina = nombre.get_estanque()

        except ValueError:
            print("Ese caracter no hará ninguna funcion sobre el vehiculo.\n")

    # Si queda sin bencina se le notifica al usuario y se cierra la app
    print("El auto se ha quedado sin bencina, lo sentimos!.")
    nombre.estado_final()


if __name__ == '__main__':
    main()
